/*
 * SafariExtensionHandler.swift
 * My Web Quirks Safari Extension
 *
 * Created by François Lamboley on 26/07/2018.
 * Copyright © 2018 Frizlab. All rights reserved.
 */

import SafariServices



class SafariExtensionHandler: SFSafariExtensionHandler {
	
	override func messageReceived(withName messageName: String, from page: SFSafariPage, userInfo: [String : Any]?) {
		/* This method will be called when a content script provided by your extension calls safari.extension.dispatchMessage("message"). */
		page.getPropertiesWithCompletionHandler{ properties in
			NSLog("The extension received a message (\(messageName)) from a script injected into (\(String(describing: properties?.url))) with userInfo (\(userInfo ?? [:]))")
		}
	}
	
}
